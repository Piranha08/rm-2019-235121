import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random as rnd
from matplotlib.path import Path
from matplotlib.patches import PathPatch


delta = .1
x_size = 10
y_size = 10
obst_vect = [(rnd.randint(-10,10), rnd.randint(-10,10)),
             (rnd.randint(-10,10), rnd.randint(-10,10)),
             (rnd.randint(-10,10),rnd.randint(-10,10)),
             (rnd.randint(-10,10),rnd.randint(-10,10))]
start_point=(-10,rnd.randint(-10,10))
finish_point=(10,rnd.randint(-10,10))

def qDist(q1,q2):
    return np.sqrt((q1[0] - q2[0])**2 + (q1[1] - q2[1])**2)

def Up(qr,qk,kp=2):
    return 0.5*kp*(qDist(qr,qk)**2)

def Fp(qr,qk,kp=1):
    return kp*qDist(qr,qk)

def Vo(qr,qo,ko=1,d0=40):
    if qDist(qr,qo)>d0:
        return 0
    if qDist(qr,qo)==0:
        return 0
    return .5*(ko*(1/qDist(qr,qo)-1/d0)**2)

def Fo(qr,qo,ko=1,d0=40):
    if qDist(qr,qo)>d0:
        return 0
    if qDist(qr,qo)==0:
        return 0
    return -ko*(1/qDist(qr,qo)-1/d0)*(1/(qDist(qr,qo)**2))

def TFo(qr,tp,ko=1,d0=40):
    TFp=0
    for p in tp:
        TFp += (Fo(qr,p,ko,d0))
    return TFp

def Vec(q1,q2):
    return (q1[0]-q2[0],q1[1]-q2[1])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)
TFp = TFo (start_point,obst_vect)
TF1 = list()
fmp = list()

n=0
for pointy in y:
    for pointx in x:
        n+=1
        zpointy=int(pointy/delta+1/delta*10)
        zpointx=int(pointx/delta+1/delta*10)
        Z[zpointy,zpointx] = TFo((pointx,pointy), obst_vect, ko=1,d0=40) + Fp((pointx,pointy),finish_point, kp= .015)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
