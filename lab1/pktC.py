import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random as rnd
from matplotlib.path import Path
from matplotlib.patches import PathPatch


delta = .1
x_size = 10
y_size = 10


obst_vect = [(rnd.randint(-10,10), rnd.randint(-10,10)),
             (rnd.randint(-10,10), rnd.randint(-10,10)),
              (rnd.randint(-10,10),rnd.randint(-10,10)),
             (rnd.randint(-10,10),rnd.randint(-10,10))]
start_point=[-10,rnd.randint(-10,10)]
finish_point=(10,rnd.randint(-10,10))

def qDist(q1,q2):
    return np.sqrt((q1[0] - q2[0])**2 + (q1[1] - q2[1])**2)

def Up(qr,qk,kp=2):
    return 0.5*kp*(qDist(qr,qk)**2)

def Fp(qr,qk,kp=1):
    return kp*qDist(qr,qk)

def Vo(qr,qo,ko=1,d0=40):
    if qDist(qr,qo)>d0:
        return 0
    if qDist(qr,qo)==0:
        return 0
    return .5*(ko*(1/qDist(qr,qo)-1/d0)**2)

def Fo(qr,qo,ko=1,d0=40):
    if qDist(qr,qo)>d0:
        return 0
    if qDist(qr,qo)==0:
        return 0
    return -ko*(1/qDist(qr,qo)-1/d0)*(1/(qDist(qr,qo)**2))

def TFo(qr,tp,ko=1,d0=40):
    TFp=0
    for p in tp:
        TFp += (Fo(qr,p,ko,d0))
    return TFp

def TFor(qr, tp, ko=3, d0=50):
    Tfp=0
    for p in tp:
        if qDist(qr,p)<dr:
            TFp += (Fo(qr,p,ko,d0))
    return TFp

def Vec(q1,q2):
    return (q1[0]-q2[0],q1[1]-q2[1])


def versor(qr,p):
    result=[p[0]-qr[0],p[1]-qr[1]]
    return result/qDist(qr,p)

def DirectionOfForce2(actual_point,finish_point,kp,obstacles,ko,d0):
    vector=0;
    for p in obstacles:
        if qDist(actual_point,p)<d0:
            vector-=(Fo(actual_point,p,ko,d0))*(versor(actual_point,p))
    vector-= Fp(actual_point,finish_point,kp)*versor(actual_point,finish_point)
 #   print(vector)
    return vector

def makeStep(qr,vect):
    a=np.degrees(np.tan(vect[1]/vect[0]))
    step=0.01
    q=qr
    if a<=22.5 and a>=-22.5:
        q[0]+=step
    if a>22.5 and a<67.5:
        q[0]+=step
        q[1]+=step
    if a>=67.5 and a<=112.5:
        q[1]+=step
    if a>-67.5 and a<-22.5:
        q[0]+=step
        q[1]-=step
    if a<=-67.5 and a>=-112.5:
        q[1]-=step
    if a<-112.5 and a>-157.5:
        q[0]-=step
        q[1]-=step
    if a>112.5 and a<157.5:
        q[0]-=step
        q[1]+=step
    if a>=157.5:
        q[0]-=step
    if a<=-157.5:
        q[0]-=step

    return q    
    
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)
TFp = TFo (start_point,obst_vect)
TF1 = list()
fmp = list()

n=0

for pointy in y:
    for pointx in x:
        n+=1
        zpointy=int(pointy/delta+1/delta*10)
        zpointx=int(pointx/delta+1/delta*10)
        Z[zpointy,zpointx] = TFo((pointx,pointy), obst_vect, ko=3,d0=40) + Fp((pointx,pointy),finish_point, kp= .04)
actual_point=start_point
moves=list()
kx=0.04
while(actual_point[0]<9.98)or((finish_point[1]-actual_point[1])>0.2)or((finish_point[1]-actual_point[1])<-0.2):
#for x in range(0,200):
    actual_point=makeStep(actual_point,DirectionOfForce2(start_point, finish_point,kx,obst_vect,ko=3,d0=40))
 #   print(actual_point)
    if actual_point[0]>10:
        actual_point[0]=10
    print(actual_point)
 #   print(finish_point)
 #   print(obst_vect)
    moves.append((actual_point[0],actual_point[1]))


kx=0.03
a=(DirectionOfForce2(start_point, finish_point, kx, obst_vect, ko=1, d0=40))
#print(np.degrees(np.tan(a[1]/a[0])))
        
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')
#print(moves)
for path in moves:
    plt.plot(path[0],path[1], "or", color='blue')
plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
